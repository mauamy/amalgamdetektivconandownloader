module amalgamDCLoader

go 1.21

toolchain go1.23.6

require (
	github.com/antchfx/htmlquery v1.0.0
	github.com/canhlinh/hlsdl v0.0.0-20201215080018-390c681bd5db
	github.com/dustin/go-humanize v1.0.0
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/mholt/archiver v3.1.1+incompatible
	github.com/urfave/cli v1.21.0
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297
)

require (
	github.com/antchfx/xpath v1.0.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a // indirect
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/frankban/quicktest v1.11.3 // indirect
	github.com/go-openapi/errors v0.19.2 // indirect
	github.com/go-openapi/strfmt v0.19.2 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/grafov/m3u8 v0.11.1 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/nwaples/rardecode v1.0.0 // indirect
	github.com/pierrec/lz4 v2.2.7+incompatible // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/ulikunitz/xz v0.5.6 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	go.mongodb.org/mongo-driver v1.0.3 // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/cheggaaa/pb.v1 v1.0.28 // indirect
)
