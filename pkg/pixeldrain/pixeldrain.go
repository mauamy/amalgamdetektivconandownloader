package pixeldrain

import (
	"fmt"
	"strings"
)

func GetDownloadUrl(pixeldrainURL string) string {
	// https://pixeldrain.com/u/G6C9aeUH --> https://pixeldrain.com/api/file/G6C9aeUH?download
	urlSplit := strings.Split(pixeldrainURL, "/")
	pixeldrainId := urlSplit[len(urlSplit)-1]
	return fmt.Sprintf("https://pixeldrain.com/api/file/%s?download", pixeldrainId)
}
