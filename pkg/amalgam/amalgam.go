package amalgam

import (
	"amalgamDCLoader/pkg/compression"
	"amalgamDCLoader/pkg/models"
	"amalgamDCLoader/pkg/pixeldrain"
	"amalgamDCLoader/pkg/web"
	"fmt"
	"github.com/antchfx/htmlquery"
	"log"
	"os"
	"strings"
)

//const amalgamBaseUrl = "https://cdn.amalgam-fansubs.moe"

const amalgamBaseUrl = "https://ddl.amalgam-fansubs.moe/content/Conan/1080p"

func getEpisodeLink(epNr string) string {
	return fmt.Sprintf("%s/[Totto]DetektivConan-%s-RFCT-[1080p].mp4", amalgamBaseUrl, epNr)
}

func getM3u8Link(epNr string) string {
	return fmt.Sprintf("https://cdn.amalgam-fansubs.moe/detektiv-conan/%s/master.m3u8", epNr)
}

func DownloadEpisode(episode *models.Episode, outputDir string) error {
	filepath := fmt.Sprintf("%v/%s", outputDir, episode.GetFilename())
	err := web.DownloadFile(episode.DownloadLink, filepath)

	return err
}

func DownloadEpisodeFromM3U8(episode *models.Episode, outputDir string) error {
	filepath := fmt.Sprintf("%v/%s", outputDir, episode.GetFilename())
	tsTmpDir := fmt.Sprintf("%s/tsdir", outputDir)

	dlFilename, err := web.DownloadM3U8(episode.M3U8Link, tsTmpDir)
	if err != nil {
		return err
	}
	// Rename and move the resulting video.ts file
	err = os.Rename(dlFilename, filepath)
	if err != nil {
		return err
	}

	// delete the temporary download directory
	err = os.Remove(tsTmpDir)
	return err
}

func DownloadFromPixeldrain(episode *models.Episode, outputDir string) error {
	downloadTmpDir := fmt.Sprintf("%s/tmpdir", outputDir)
	downloadFilePath := fmt.Sprintf("%s/video.rar", downloadTmpDir)

	// Download rar archive into temp directory
	os.MkdirAll(downloadTmpDir, 0755)
	pixeldrainDownloadUrl := pixeldrain.GetDownloadUrl(episode.PixeldrainLink)
	err := web.DownloadFile(pixeldrainDownloadUrl, downloadFilePath)
	if err != nil {
		return err
	}

	filepath := fmt.Sprintf("%v/%s", outputDir, episode.GetFilename())
	errMsg, err := compression.Unrar(downloadFilePath, filepath)
	if err != nil {
		log.Println(errMsg)
		return err
	}

	os.RemoveAll(downloadTmpDir)

	return nil
}

func FetchEpisodes() ([]*models.Episode, error) {
	defaultBaseUrl := "https://amalgam-fansubs.moe"
	backupBaseUrl := "https://amalgamsubs.lima-city.de"
	var baseUrl string

	if web.CheckIfAvailable(defaultBaseUrl) {
		baseUrl = defaultBaseUrl
	} else {
		fmt.Printf("%v is not reachable right now, trying to use backup %v:\n", defaultBaseUrl, backupBaseUrl)
		baseUrl = backupBaseUrl
	}

	urls := []string{
		baseUrl + "/detektiv-conan/",
		baseUrl + "/detektiv-conan-2018/",
	}

	var episodes []*models.Episode
	for _, url := range urls {
		doc, err := htmlquery.LoadURL(url)
		if err != nil {
			return nil, err
		}

		conanDiv := htmlquery.FindOne(doc, "//div[@id='conan']")
		if conanDiv == nil {
			continue
		}
		episodeTable := htmlquery.FindOne(conanDiv, "table")
		if episodeTable == nil {
			continue
		}

		rows := htmlquery.Find(episodeTable, "//tr")

		for i := 1; i < len(rows); i++ {
			cols := htmlquery.Find(rows[i], "//td")
			if len(cols) <= 0 {
				continue
			}
			episodeNr := htmlquery.InnerText(cols[0]) // number
			episodeNr = strings.ReplaceAll(episodeNr, ".", "")
			episodeTitle := htmlquery.InnerText(cols[1]) // title

			pixeldrainLink := htmlquery.SelectAttr(cols[3].FirstChild, "href") // pixeldrain link
			if !strings.Contains(pixeldrainLink, "pixeldrain.com") {
				pixeldrainLink = ""
			}

			downloadLink := getEpisodeLink(episodeNr)

			m3u8Link := getM3u8Link(episodeNr)

			episodes = append(episodes, &models.Episode{
				Title:          episodeTitle,
				EpisodeNr:      strings.TrimSpace(episodeNr),
				M3U8Link:       m3u8Link,
				PixeldrainLink: pixeldrainLink,
				DownloadLink:   downloadLink,
			})
		}
	}

	return episodes, nil
}
