package compression

import (
	"bytes"
	"os"
	"os/exec"
)

func Unrar(rarArchivePath, targetFilePath string) (string, error) {
	cmd := exec.Command("unrar", "p", rarArchivePath)
	var outb, errb bytes.Buffer

	cmd.Stdout = &outb
	cmd.Stderr = &errb

	err := cmd.Run()
	if err != nil {
		return errb.String(), err
	}
	//fmt.Println("out:", outb.String(), "err:", errb.String())
	err = os.WriteFile(targetFilePath, outb.Bytes(), 0644)
	if err != nil {
		return errb.String(), err
	}

	return "", nil
}
